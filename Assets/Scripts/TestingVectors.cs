using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestingVectors : MonoBehaviour
{
    [SerializeField]
    private Vector3 objectVector = Vector3.up;
    [SerializeField]
    private Color objectColor = Color.red;
    [SerializeField]
    private LineRenderer objectLine;
    [SerializeField]
    private float lineWidth = 0.1f;
    

    [SerializeField]
    private Transform objectOrigin;


    [Space(10)]
    [SerializeField]
    private Vector3 lightVector = new Vector3(1, -1, 0);
    [SerializeField]
    private Color lightColor = Color.yellow;
    [SerializeField]
    private LineRenderer lightLine;

    [Space(10)]
    [SerializeField]
    private Vector3 groundVector = new Vector3(5, 0, 0);
    [SerializeField]
    private Color groundColor = Color.green;
    [SerializeField]
    private LineRenderer groundLine;

    private void OnDrawGizmos()
    {


        // gizmos take origin and direction
        // Gizmos.DrawCube(Vector3.zero, Vector3.one);
        // Gizmos.color = objectColor;
        // Gizmos.DrawRay(transform.position, transform.up);
        // Gizmos.DrawRay(transform.position, transform.TransformVector(objectVector));
        // linerenderers take positions (not directions)
        // transform vector applies rotation but it does not apply current translation
        // tranform direction applies rotation but also it does not apply current translation
        // transform point applies also the translation
        Vector3 objectEndPoint = transform.TransformPoint(objectVector); //WS
        objectLine.SetPosition(0, transform.position);
        objectLine.SetPosition(1, objectEndPoint);
        objectLine.startWidth = lineWidth;
        objectLine.endWidth = lineWidth;
        objectLine.startColor = objectColor;
        objectLine.endColor = objectColor;


        Vector3 normalizedLight = lightVector.normalized;
        Vector3 lightEndPoint = objectEndPoint + transform.TransformDirection(normalizedLight);
        // Gizmos.color = lightColor;
        // Gizmos.DrawRay(transform.TransformPoint(objectVector), transform.TransformDirection(normalizedLight));
        lightLine.startWidth = lineWidth;
        lightLine.endWidth = lineWidth;
        lightLine.startColor = lightColor;
        lightLine.endColor = lightColor;
        lightLine.SetPosition(0, objectEndPoint);
        lightLine.SetPosition(1, lightEndPoint);

        groundLine.startWidth = lineWidth;
        groundLine.endWidth = lineWidth;
        groundLine.startColor = groundColor;
        groundLine.endColor = groundColor;

        // Gizmos.color = groundColor;
        Vector3 groundEndPoint = transform.position + transform.TransformDirection(groundVector);
        // Gizmos.DrawRay(transform.position, transform.up);
        // Gizmos.DrawRay(transform.position, transform.TransformVector(groundVector));
        groundLine.SetPosition(0, transform.position);
        groundLine.SetPosition(1, groundEndPoint);
    }
}
