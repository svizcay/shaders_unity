using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class ProcedurealMesh : MonoBehaviour
{
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    public class VertexData
    {
        public Vector3 pos;
        public Vector2 uv;
    }

    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        Mesh proceduralMesh = new Mesh();
        proceduralMesh.name = "procedural mesh";

        meshFilter.mesh = proceduralMesh;

        List<VertexData> vertices = new List<VertexData>();
        
        VertexData vertexA = new VertexData();
        vertexA.pos = new Vector3(-1, -1, 0);
        vertexA.uv = new Vector2(0, 0);

        VertexData vertexB = new VertexData();
        vertexB.pos = new Vector3(1, -1, 0);
        vertexB.uv = new Vector2(1, 0);

        VertexData vertexC = new VertexData();
        vertexC.pos = new Vector3(0, 1, 0);
        vertexC.uv = new Vector2(0.5f, 1);

        vertices.Add(vertexA);
        vertices.Add(vertexB);
        vertices.Add(vertexC);

        Vector3[] arrayOfVertices = new Vector3[vertices.Count];
        Vector2[] arrayOfUVs = new Vector2[vertices.Count];
        for (int i = 0; i < vertices.Count; i++)
        {
            arrayOfVertices[i] = vertices[i].pos;
        }

        for (int i = 0; i < vertices.Count; i++)
        {
            arrayOfUVs[i] = vertices[i].uv;
        }

        proceduralMesh.vertices = arrayOfVertices;
        proceduralMesh.uv = arrayOfUVs;

        proceduralMesh.triangles = new int[] { 0, 2, 1 }; // A C B i.e. clockwise

        
    }
}
