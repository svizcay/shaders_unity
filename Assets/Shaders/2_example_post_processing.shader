Shader "Own/2_example_post_processing"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _ExtraTex ("extra texture", 2D) = "white" {}
		width ("width", Range (0, 0.5)) = 0.005
		x ("x", Range (0, 1)) = 0.5
		y ("y", Range (0, 1)) = 0.5
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID // single pass stereo needs instancing enabled
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
                float4 vertex : SV_POSITION;
				UNITY_VERTEX_OUTPUT_STEREO // to enable single pass stereo
            };

            //float4 _MainTex_ST; // needed for TRANSFORM_TEX

            v2f vert (appdata v)
            {
                v2f o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o); // for single pass stereo
                o.vertex = UnityObjectToClipPos(v.vertex);
                // not sure if next line needs to be replaced
                o.uv = v.uv;
                //o.uv2 = v.uv;
                //o.uv2 = ComputeScreenPos(o.vertex) ;
                o.uv2 = ComputeGrabScreenPos (o.vertex) ;
                // o.uv = TRANSFORM_TEX(v.uv, _MainTex); // using this shows pink error
                return o;
            }

            // sampler2D _MainTex; // replaced by next line
			UNITY_DECLARE_SCREENSPACE_TEXTURE(_MainTex); //Insert
			//UNITY_DECLARE_SCREENSPACE_TEXTURE(_ExtraTex); //Insert

            sampler2D _ExtraTex; // replaced by next line

            uniform float width;
            uniform float x;
            uniform float y;

            fixed4 frag (v2f i) : SV_Target
            {
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(i); //Insert

                // transformed uv to have 1 quad full black
                float2 uv_2 = i.uv2 * 2 - 1;

                // draw a line to divide the quads
                //float width = 0.2f;
                float uv_vertical_center = x * 2 - 1;
                float uv_horizontal_center = y * 2 - 1;
                if (abs(uv_2.x - uv_vertical_center) < width)
                {
                    return fixed4(0, 0, 1, 1);
				}

                if (abs(uv_2.y - uv_horizontal_center) < width)
                {
                    return fixed4(0, 0, 1, 1);
				}

                fixed4 col = tex2D(_ExtraTex, i.uv);
				//fixed4 col = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_ExtraTex, i.uv);
                return col;

                return fixed4(uv_2, 0, 1);

                // fixed4 col = tex2D(_MainTex, i.uv);
                // // just invert the colors
                // col.rgb = 1 - col.rgb;
                // return col;
            }
            ENDCG
        }
    }
}
